variable "access_key" {
  description = "The access key"
  type        = string
  sensitive   = true
}

variable "secret_key" {
  description = "The secret key"
  type        = string
  sensitive   = true
}