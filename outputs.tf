output "access_key" {
  value = var.access_key
  sensitive = true
}

output "secret_key" {
  value = var.secret_key
  sensitive = true
}

output "environment" {
  value = local.env
}

output "instance_type" {
  value = local.instance_type
}