locals {
  env = terraform.workspace
  counts = {
    default    = 1
    production = 3
  }
  instances = {
    default    = "t2.micro"
    production = "t4.large"
  }
  common_configurations = {
    default = {
      region                      = "us-east-1"
      s3_force_path_style         = true
      skip_credentials_validation = true
      skip_metadata_api_check     = true
      skip_requesting_account_id  = true
      dynamodb_endpoint           = "http://172.19.129.20:4569"
      ec2_endpoint                = "http://172.19.129.20:4597"
    }
    production = {
      region                      = "us-east-1"
      s3_force_path_style         = false
      skip_credentials_validation = false
      skip_metadata_api_check     = false
      skip_requesting_account_id  = false
      dynamodb_endpoint           = "http://amazon.com/dynamodb_endpoint:4569"
      ec2_endpoint                = "http://amazon.com/ec2_endpoint:4597"
    }
  }
  instance_type        = lookup(local.instances, local.env)
  count                = lookup(local.counts, local.env)
  common_configuration = lookup(local.common_configurations, local.env)
}