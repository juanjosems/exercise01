provider "aws" {
  access_key                  = var.access_key
  secret_key                  = var.secret_key
  alias                       = "us-east-1"
  region                      = local.common_configuration.region
  s3_force_path_style         = local.common_configuration.s3_force_path_style
  skip_credentials_validation = local.common_configuration.skip_credentials_validation
  skip_metadata_api_check     = local.common_configuration.skip_metadata_api_check
  skip_requesting_account_id  = local.common_configuration.skip_requesting_account_id

  endpoints {
    dynamodb = local.common_configuration.dynamodb_endpoint
    ec2      = local.common_configuration.ec2_endpoint
  }
}

resource "aws_instance" "web" {
  ami           = "ami-0d57c0143330e1fa7"
  instance_type = local.instance_type
  provider = aws.us-east-1
  tags = {
    Name = "HelloWorld_${local.env}"
  }
}

resource "aws_dynamodb_table" "us-east-1" {
  provider = aws.us-east-1

  hash_key         = "myAttribute"
  name             = "myTable"
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  read_capacity    = 1
  write_capacity   = 1

  attribute {
    name = "myAttribute"
    type = "S"
  }
}
//
//resource "aws_dynamodb_global_table" "myTable" {
//  depends_on = [
//    aws_dynamodb_table.us-east-1
//  ]
//  provider = aws.us-east-1
//
//  name = "myTable"
//
//  replica {
//    region_name = "us-east-1"
//  }
//}
