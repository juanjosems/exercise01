# Cloud Native Landing Challenge - Exercise 01

This project is an example of Infrastructure as code that using Terraform Script should be able to deploy and EC2 instance and a DynamoDB Database.

## Begining 🚀

_This manual guide allow you to obtain a copy of the project and deploy it in a development environment._

_How to clone project_

```
git clone https://juanjosems@bitbucket.org/juanjosems/exercise01.git
```

### Pre-requisites 📋

_docker must be installed and running in a local or a remote machine_

### Installing 🔧

_How to install localstack_
```
docker run -it -p 4500-4600:4500-4600 -p 8080:8080 --expose 4572 localstack/localstack:0.11.0
To test it, open a webbrowser and navigate to http://docker_host_ip:4566
If the container is running you will recive {"status": "running"}
```
_How to install and configure Terraform_
```
Navigate to Terraform's official website and download Terraform version 1.0.6 or if you have Windows System, 
you can use https://releases.hashicorp.com/terraform/1.0.6/terraform_1.0.6_windows_amd64.zip
Once your download is finished, unzip Terraform in C:\software\devtools\terraform\1.0.6
Define an environment variable TERRAFORM_HOME and set it C:\software\devtools\terraform\1.0.6
Change your path environment variable adding TERRAFORM_HOME
Open command line and check the process is ok using 'terraform -v'
```

## Running Tests ⚙️

_How to test the project_
```
Edit locals.tf file and change 172.19.129.20 to other valid IP where LocalStack is running
Open command line and navigate to folder where you cloned the project
```
_To start with Terraform execute_
```
terraform init
```
_Ensure workspace selected is default_
```
terraform workspace new production
terraform workspace select default
```
_To show execution plan execute_
```
terraform plan -var-file=default.tfvars -out=test.plan
```
_To apply plan execute_
```
terraform apply -auto-approve "test.plan"
```

_To destroy infrastructure execute_
```
terraform destroy "test.plan"
```
## Built with 🛠️

_There are the tools used to build the project_

* [Terraform](https://www.terraform.io/)
* [LocalStack] (https://docs.localstack.cloud/get-started/#docker)
* [Docker] (https://www.docker.com/)
* [gitignore file online generator](https://www.toptal.com/developers/gitignore)

## Decisions and security considerations 📖

### Decisions
- I used Docker because of I had already a Docker platform installed in my computer and for was faster to install LocalStack.
- I decided use Terraform workspaces because of allow me to separate some kinds of environments (development, production, ...) and you can easially create more.
- Following Terraform best practices I decided separate Terraform configuration files in variables, main, versions, locals and outputs
  main.tf: Contains the main code
  variables.tf: Contains the variables
  versions.tf: To fix both dependency and terraform versions
  outputs.tf: To show outputs when plan is displayed
  local.tf: To define constants  
- access_key and secret_key are marked as sensitive in variables.tf to avoid show information when plan is displayed
- You must pass access_key and secret_key as variables when you apply the plan.

### Security considerations
- Environment variables is not enough. But is better than plain text.
- Store Terraform state in a backend that support encrypting. For example S3
- Keystore service like AWS KMS to encrypt secrets.
- Use of AWS-managed secret store.

## Author ✒️
* **Juanjo Muñoz** - *Trabajo Inicial* - [juanjosems](https://juanjosems@bitbucket.org)

## License 📄

This project is free license
---
